# Installation

This package requires an HTTPlug implementation in order to work properly. Before installing this package, check out the [HTTPlug for library users](http://docs.php-http.org/en/latest/httplug/users.html) page.

## TL;DR

```
composer require php-http/curl-client guzzlehttp/psr7 php-http/message
composer require paybetr/paybetr-php
```

Check out the [full documentation.](https://dbdnet.atlassian.net/wiki/spaces/PUB/pages/63438859/Paybetr+PHP+Library)
