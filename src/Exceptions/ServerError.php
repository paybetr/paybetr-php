<?php

namespace Paybetr\PaybetrPHP\Exceptions;

/**
 * exception class for server errors (5xx)
 */
class ServerError extends \Exception
{
}
