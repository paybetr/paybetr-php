<?php

namespace Paybetr\PaybetrPHP\Exceptions;

/**
 * exception class for client errors (4xx)
 */
class ClientError extends \Exception
{
}
