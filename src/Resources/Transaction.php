<?php

namespace Paybetr\PaybetrPHP\Resources;

class Transaction extends Resource
{
    /**
     * get the identifier field name for 'find' queries
     * @return string
     */
    public function getIdentifier()
    {
        return 'txid';
    }

    /**
     * find a resource by it's identifier
     * @return object
     */
    public function find($identifier)
    {
        $this->client->setUri('transactions/' . $identifier);
        return $this->client->request();
    }

    /**
     * get a collection of resources
     * @return object
     */
    public function get(array $arguments = [])
    {
        if (isset($arguments['symbol'])) {
            $this->client->setUri('currencies/' . $arguments['symbol'] . '/transactions');
        } else {
            $this->client->setUri('transactions');
        }
        return $this->client->request();
    }
}
