<?php

namespace Paybetr\PaybetrPHP\Resources;

use Paybetr\PaybetrPHP\Exceptions\ClientError;

class Trigger extends Resource
{
    /**
     * get the identifier field name for 'find' queries
     * @return string
     */
    public function getIdentifier()
    {
        return 'id';
    }

    /**
     * find a resource by it's identifier
     * @return object
     */
    public function find($identifier)
    {
        $this->client->setUri('triggers/' . $identifier);
        return $this->client->request();
    }

    /**
     * get a collection of resources
     * @return object
     */
    public function get(array $arguments = [])
    {
        if (isset($arguments['webhook_id'])) {
            $this->client->setUri('webhooks/' . $arguments['webhook_id'] . '/triggers');
        } else {
            $this->client->setUri('triggers');
        }
        return $this->client->request();
    }

    /**
     * create a resource
     * @return object
     */
    public function create(array $attributes = [])
    {
        if (!isset($attributes['webhook_id'])) {
            throw new ClientError("Required parameter 'webhook_id' not provided.", 400);
        }
        if (!isset($attributes['trigger'])) {
            throw new ClientError("Required parameter 'trigger' not provided.", 400);
        }
        $this->client->setUri('webhooks/' . $attributes['webhook_id'] . '/triggers');
        return $this->client->request('POST', ['trigger' => $attributes['trigger']]);
    }

    /**
     * update a resource
     * @param array $attributes - the resource attributes
     * @return object
     */
    public function update(array $attributes = [])
    {
        if (!$this->getId()) {
            throw new ClientError('Object has not been populated yet.', 400);
        }
        $this->client->setUri('triggers/' . $this->getId());
        return $this->client->request('PUT', $attributes);
    }

    /**
     * delete a resource
     * @return void
     */
    public function delete()
    {
        if (!$this->getId()) {
            throw new ClientError('Object has not been populated yet.', 400);
        }
        $this->client->setUri('triggers/' . $this->getId());
        $this->client->request('DELETE');
        return;
    }
}
