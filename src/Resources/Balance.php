<?php

namespace Paybetr\PaybetrPHP\Resources;

class Balance extends Resource
{
    /**
     * get the identifier field name for 'find' queries
     * @return string
     */
    public function getIdentifier()
    {
        return 'symbol';
    }

    /**
     * find a resource by it's identifier
     * @return object
     */
    public function find($identifier)
    {
        $this->client->setUri('currencies/' . $identifier . '/balance');
        return $this->client->request();
    }

    /**
     * get a collection of resources
     * @return object
     */
    public function get(array $arguments = [])
    {
        $this->client->setUri('balances');
        return $this->client->request();
    }
}
