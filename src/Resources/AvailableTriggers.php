<?php

namespace Paybetr\PaybetrPHP\Resources;

class AvailableTriggers extends Resource
{
    /**
     * find a resource by it's identifier
     * @return object
     */
    public function find(string $identifier = null)
    {
        $this->client->setUri('availabletriggers');
        return $this->client->request();
    }

    /**
     * get a collection of resources
     * @return object
     */
    public function get(array $arguments = [])
    {
        $this->client->setUri('availabletriggers');
        return $this->client->request();
    }
}
