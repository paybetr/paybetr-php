<?php

namespace Paybetr\PaybetrPHP\Resources;

class Currency extends Resource
{
    /**
     * get the identifier field name for 'find' queries
     * @return string
     */
    public function getIdentifier()
    {
        return 'symbol';
    }

    /**
     * find a resource by it's identifier
     * @return object
     */
    public function find(string $identifier)
    {
        $this->client->setUri('currencies/' . $identifier);
        return $this->client->request();
    }

    /**
     * get a collection of resources
     * @return object
     */
    public function get(array $arguments = [])
    {
        $this->client->setUri('currencies');
        return $this->client->request();
    }

    /**
     * convert currency
     * @return object
     */
    public function convert(array $arguments = [])
    {
        if (!isset($arguments['amount'])) {
            $arguments['amount'] = 1;
        }
        $this->client->setUri('currencies/' . $arguments['symbol_from'] . '/convert/' . $arguments['symbol_to'] . '/' . $arguments['amount']);
        return $this->client->request();
    }
    
    /**
     * buy currency
     * @param array $arguments
     * @return object
     */
    public function buy(array $arguments = [])
    {
        $this->client->setUri('moonpay?' . http_build_query($arguments));
        return $this->client->request();
    }
}
