<?php

namespace Paybetr\PaybetrPHP\Resources;

use Paybetr\PaybetrPHP\Client;
use Paybetr\PaybetrPHP\Exceptions\ClientError;

class Exchange extends Resource
{
    public function __construct(Client $client, $resource = null)
    {
        parent::__construct($client, $resource);
        if (!is_object($this->attributes)) {
            return;
        }
        if (isset($this->attributes->from_transaction)) {
            $this->attributes->from_transaction = Transaction::resource($this->client, $this->attributes->from_transaction);
        }
        if (isset($this->attributes->to_transaction)) {
            $this->attributes->to_transaction = Transaction::resource($this->client, $this->attributes->to_transaction);
        }
    }

    /**
     * find a resource by it's identifier
     * @return object
     */
    public function find(string $identifier)
    {
        $this->client->setUri('exchanges/' . $identifier);
        return $this->client->request();
    }

    /**
     * get a collection of resources
     * @return object
     */
    public function get(array $arguments = [])
    {
        $this->client->setUri('exchanges');
        return $this->client->request();
    }

    /**
     * create a resource
     * @return object
     */
    public function create(array $attributes = [])
    {
        if (!isset($attributes['from_symbol'])) {
            throw new ClientError("Required attribute 'from_symbol' not provided.", 400);
        }
        if (!isset($attributes['to_symbol'])) {
            throw new ClientError("Required symbol 'to_symbol' not provided.", 400);
        }
        if (!isset($attributes['amount'])) {
            throw new ClientError("Required attribute 'amount' not provided.", 400);
        }
        $this->client->setUri('currencies/' . $attributes['from_symbol'] . '/exchange/' . $attributes['to_symbol'] . '/' . $attributes['amount'] . (isset($attributes['external_id']) ? '/' . $attributes['external_id'] : null));
        return $this->client->request();
    }
}
