<?php

namespace Paybetr\PaybetrPHP\Resources;

use Paybetr\PaybetrPHP\Exceptions\ClientError;

class Webhook extends Resource
{
    /**
     * get the identifier field name for 'find' queries
     * @return string
     */
    public function getIdentifier()
    {
        return 'id';
    }

    /**
     * find a resource by it's identifier
     * @return object
     */
    public function find($identifier)
    {
        $this->client->setUri('webhooks/' . $identifier);
        return $this->client->request();
    }

    /**
     * get a collection of resources
     * @return object
     */
    public function get(array $arguments = [])
    {
        $this->client->setUri('webhooks');
        return $this->client->request();
    }

    /**
     * create a resource
     * @return object
     */
    public function create(array $attributes = [])
    {
        if (!isset($attributes['url'])) {
            throw new ClientError("Required parameter 'url' not provided.", 400);
        }
        $this->client->setUri('webhooks');
        return $this->client->request('POST', $attributes);
    }

    /**
     * update a resource
     * @param array $attributes - the resource attributes
     * @return object
     */
    public function update(array $attributes = [])
    {
        if (!$this->getId()) {
            throw new ClientError('Object has not been populated yet.', 400);
        }
        $this->client->setUri('webhooks/' . $this->getId());
        return $this->client->request('PUT', $attributes);
    }

    /**
     * delete a resource
     * @return void
     */
    public function delete()
    {
        if (!$this->getId()) {
            throw new ClientError('Object has not been populated yet.', 400);
        }
        $this->client->setUri('webhooks/' . $this->getId());
        $this->client->request('DELETE');
        return;
    }
}
