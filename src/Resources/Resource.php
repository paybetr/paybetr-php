<?php

namespace Paybetr\PaybetrPHP\Resources;

use Paybetr\PaybetrPHP\Interfaces\ResourceInterface;
use Paybetr\PaybetrPHP\Client;
use Paybetr\PaybetrPHP\Exceptions\ClientError;

/**
 * the resource class
 */
abstract class Resource implements ResourceInterface
{
    /**
     * the paybetr client object
     * @property Paybetr\PaybetrPHP\Client $client
     */
    protected $client;

    /**
     * the id of the resource
     * @property string $id
     */
    public $id;

    /**
     * the resource attributes
     * @property object $attributes
     */
    public $attributes;

    /**
     * class constructor
     * @param Paybetr\PaybetrPHP\Client $client - the paybetr api client
     * @param object resource - the resource
     */
    public function __construct(Client $client, $resource = null)
    {
        $this->client = $client;
        $this->id = isset($resource->id) ? $resource->id : null;
        $this->attributes = isset($resource->attributes) ? $resource->attributes : null;
    }

    /**
     * static constructor
     * @param Paybetr\PaybetrPHP\Client $client - the Paybetr client class
     * @param object $resource - the resource
     * @return Paybetr\PaybetrPHP\Resources\Resource
     */
    public static function resource(Client $client, $resource)
    {
        return new static($client, $resource);
    }

    /**
     * return the id of the resource
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * return the attributes of the resource
     * @return object
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * get the identifier field name for 'find' queries
     * @return string
     */
    public function getIdentifier()
    {
        return 'id';
    }

    /**
     * convert the resource to an array
     * @return array
     */
    public function toArray()
    {
        return json_decode(json_encode($this), true);
    }

    /**
     * create a resource
     * @param array $attributes - the resource attributes
     * @return object
     */
    public function create(array $attributes = [])
    {
        throw new ClientError('Create method is not implemented.', 400);
    }

    /**
     * update a resource
     * @param array $attributes - the resource attributes
     * @return object
     */
    public function update(array $attributes = [])
    {
        throw new ClientError('Update method is not implemented.', 400);
    }

    /**
     * delete a resource
     * @return void
     */
    public function delete()
    {
        throw new ClientError('Delete method is not implemented.', 400);
    }
}
