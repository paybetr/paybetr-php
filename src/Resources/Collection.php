<?php

namespace Paybetr\PaybetrPHP\Resources;

use Paybetr\PaybetrPHP\Interfaces\CollectionInterface;
use Paybetr\PaybetrPHP\Client;

/**
 * the collection class
 */
class Collection implements CollectionInterface
{
    /**
     * Paybetr\PaybetrPHP\Client object
     */
    public $client;

    /**
     * array of data in the collection. Each item should
     * contain a Paybetr\PaybetrPHP\Interfaces\Resource object
     */
    public $data = [];

    /**
     * meta data returned from the api
     */
    public $meta;

    /**
     * links data returned from the api
     */
    public $links;

    /**
     * class constructor
     * @param Paybetr\PaybetrPHP\Client $client - the Paybetr client class
     * @param object $collection - the collection
     */
    public function __construct(Client $client, $collection)
    {
        $this->client = $client;
        $this->links = isset($collection->links) ? $collection->links : null;
        $this->meta = isset($collection->meta) ? $collection->meta : null;
        $this->setData(isset($collection->data) ? $collection->data : [$collection]);
    }

    /**
     * static constructor
     * @param Paybetr\PaybetrPHP\Client $client - the Paybetr client class
     * @param object $collection - the collection
     * @return Paybetr\PaybetrPHP\Resources\Collection
     */
    public static function collection(Client $client, $collection)
    {
        return new static($client, $collection);
    }

    /**
     * convert collection to an array
     * @return array
     */
    public function toArray()
    {
        return json_decode(json_encode($this), true);
    }

    /**
     * return the data from the collection
     * @return array
     */
    public function data()
    {
        return $this->data;
    }

    /**
     * go to the first page of the collection results
     * @return Paybetr\PaybetrPHP\Resources\Collection | null
     */
    public function first()
    {
        if (isset($this->links->first)) {
            $this->client->setUrl(urldecode($this->links->first));
            return $this->refresh();
        }
    }

    /**
     * go to the last page of the collection results
     * @return Paybetr\PaybetrPHP\Resources\Collection | null
     */
    public function last()
    {
        if (isset($this->links->last)) {
            $this->client->setUrl(urldecode($this->links->last));
            return $this->refresh();
        }
    }

    /**
     * go to the previous page of the collection results
     * @return Paybetr\PaybetrPHP\Resources\Collection | null
     */
    public function previous()
    {
        if (isset($this->links->prev)) {
            $this->client->setUrl(urldecode($this->links->prev));
            return $this->refresh();
        }
    }

    /**
     * go to the next page of the collection results
     * @return Paybetr\PaybetrPHP\Resources\Collection | null
     */
    public function next()
    {
        if (isset($this->links->next)) {
            $this->client->setUrl(urldecode($this->links->next));
            return $this->refresh();
        }
    }

    /**
     * sets the data of the object
     * @param object $data - the data object
     */
    private function setData($data)
    {
        $this->data = [];
        foreach ($data as $resource) {
            $this->data[] = $this->client->getResourceClass()::resource($this->client, $resource);
        }
    }

    /**
     * refreshes the collection from the api
     * @return Paybetr\PaybetrPHP\Resources\Collection
     */
    private function refresh()
    {
        $this->__construct($this->client, $this->client->request());
        return $this;
    }
}
