<?php

namespace Paybetr\PaybetrPHP\Resources;

use Paybetr\PaybetrPHP\Exceptions\ClientError;

class Address extends Resource
{
    /**
     * get the identifier field name for 'find' queries
     * @return string
     */
    public function getIdentifier()
    {
        return 'address';
    }

    /**
     * find a resource by it's identifier
     * @return object
     */
    public function find($identifier)
    {
        $this->client->setUri('addresses/' . $identifier);
        return $this->client->request();
    }

    /**
     * get a collection of resources
     * @return object
     */
    public function get(array $arguments = [])
    {
        if (isset($arguments['symbol'])) {
            $this->client->setUri('currencies/' . $arguments['symbol'] . '/addresses');
        } else {
            $this->client->setUri('addresses');
        }
        return $this->client->request();
    }

    /**
     * create a resource
     * @return object
     */
    public function create(array $attributes = [])
    {
        if (!isset($attributes['symbol'])) {
            throw new ClientError("Required attribute 'symbol' not provided.", 400);
        }
        $this->client->setUri('currencies/' . $attributes['symbol'] . '/addresses/new' . (isset($attributes['external_id']) ? '/' . $attributes['external_id'] : ''));
        return $this->client->request();
    }
}
