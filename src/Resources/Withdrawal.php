<?php

namespace Paybetr\PaybetrPHP\Resources;

use Paybetr\PaybetrPHP\Exceptions\ClientError;

class Withdrawal extends Resource
{
    /**
     * get the identifier field name for 'find' queries
     * @return string
     */
    public function getIdentifier()
    {
        return 'id';
    }

    /**
     * find a resource by it's identifier
     * @return object
     */
    public function find($identifier)
    {
        $this->client->setUri('withdrawals/' . $identifier);
        return $this->client->request();
    }

    /**
     * get a collection of resources
     * @return object
     */
    public function get(array $arguments = [])
    {
        if (isset($arguments['symbol'])) {
            $this->client->setUri('currencies/' . $arguments['symbol'] . '/withdrawals');
        } else {
            $this->client->setUri('withdrawals');
        }
        return $this->client->request();
    }

    /**
     * create a resource
     * @return object
     */
    public function create(array $attributes = [])
    {
        if (!isset($attributes['symbol'])) {
            throw new ClientError("Required attribute 'symbol' not provided.", 400);
        }
        if (!isset($attributes['to'])) {
            throw new ClientError("Required attribute 'to' not provided.", 400);
        }
        if (!isset($attributes['amount'])) {
            throw new ClientError("Required attribute 'amount' not provided.", 400);
        }
        $this->client->setUri('currencies/' . $attributes['symbol'] . '/withdrawals/request/' . $attributes['to'] . '/' . $attributes['amount'] . (isset($attributes['external_id']) ? '/' . $attributes['external_id'] : null));
        return $this->client->request();
    }

    /**
     * update a resource
     * @param array $attributes - the resource attributes
     * @return object
     */
    public function update(array $attributes = [])
    {
        if (!$this->getId()) {
            throw new ClientError('Object has not been populated yet.', 400);
        }
        if (!isset($attributes['action'])) {
            throw new ClientError("Required attribute 'action' not provided.", 400);
        }
        switch ($attributes['action']) {
            case 'confirm':
                $this->client->setUri('withdrawals/' . $this->getId() . '/confirm');
                break;
            case 'cancel':
                $this->client->setUri('withdrawals/' . $this->getId() . '/cancel');
                break;
        }
        return $this->client->request();
    }

    /**
     * delete a resource
     * @return void
     */
    public function delete()
    {
        return $this->update(['action' => 'cancel']);
    }
}
