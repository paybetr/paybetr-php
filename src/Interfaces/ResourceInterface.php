<?php

namespace Paybetr\PaybetrPHP\Interfaces;

use Paybetr\PaybetrPHP\Client;

/**
 * interface for the resource class
 */
interface ResourceInterface
{
    /**
     * static constructor
     * @param Paybetr\PaybetrPHP\Client $client - the Paybetr client class
     * @param object $resource - the resource
     * @return Paybetr\PaybetrPHP\Interfaces\ResourceInterface
     */
    public static function resource(Client $client, $resource);

    /**
     * return the id of the resource
     * @return string
     */
    public function getId();

    /**
     * return the attributes of the resource
     * @return object
     */
    public function getAttributes();

    /**
     * convert the resource to an array
     * @return array
     */
    public function toArray();

    /**
     * get the identifier field name for 'find' queries
     * @return string
     */
    public function getIdentifier();

    /**
     * find a resource by it's identifier
     * @param string $identifier - the identifier for the resource
     * @return object
     */
    public function find(string $identifier);

    /**
     * get a collection of resources
     * @param array $arguments - arguments for the query
     * @return object
     */
    public function get(array $arguments = []);

    /**
     * create a resource
     * @param array $attributes - the resource attributes
     * @return object
     */
    public function create(array $attributes = []);

    /**
     * update a resource
     * @param array $attributes - the resource attributes
     * @return object
     */
    public function update(array $attributes = []);

    /**
     * delete a resource
     * @return void
     */
    public function delete();
}
