<?php

namespace Paybetr\PaybetrPHP\Interfaces;

use Paybetr\PaybetrPHP\Client;

/**
 * Interface for the collection class
 */
interface CollectionInterface
{
    /**
     * static constructor
     * @param Paybetr\PaybetrPHP\Client $client - the Paybetr client class
     * @param object $collection - the collection
     * @return Paybetr\PaybetrPHP\Interfaces\CollectionInterface
     */
    public static function collection(Client $client, $collection);

    /**
     * convert collection to an array
     * @return array
     */
    public function toArray();

    /**
     * return the data from the collection
     * @return array
     */
    public function data();

    /**
     * go to the first page of the collection results
     * @return Paybetr\PaybetrPHP\Interfaces\CollectionInterface | null
     */
    public function first();

    /**
     * go to the last page of the collection results
     * @return Paybetr\PaybetrPHP\Interfaces\CollectionInterface | null
     */
    public function last();

    /**
     * go to the previous page of the collection results
     * @return Paybetr\PaybetrPHP\Interfaces\CollectionInterface | null
     */
    public function previous();

    /**
     * go to the next page of the collection results
     * @return Paybetr\PaybetrPHP\Interfaces\CollectionInterface | null
     */
    public function next();
}
