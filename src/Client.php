<?php

namespace Paybetr\PaybetrPHP;

use Http\Client\HttpClient;
use Http\Message\MessageFactory;
use Http\Discovery\HttpClientDiscovery;
use Http\Client\Common\HttpMethodsClient;
use Http\Discovery\MessageFactoryDiscovery;
use Paybetr\PaybetrPHP\Exceptions\ClientError;
use Paybetr\PaybetrPHP\Exceptions\ServerError;

/**
 * the paybetr client class
 */
class Client
{
    /**
     * the api token for accessing the paybetr api
     * @property string $apiToken
     */
    protected $apiToken;

    /**
     * the base url for the paybetr api
     * @property string $baseUrl
     */
    protected $baseUrl = 'https://api.paybetr.com/api/v1/';

    /**
     * the http methods client
     * @property Http\Client\Common\HttpMethodsClient $client
     */
    protected $client;

    /**
     * the query parameters for the api request
     * @property array $query
     */
    protected $query = [];

    /**
     * the api path for this request
     * @property string $uri
     */
    protected $uri = '';

    /**
     * the class used for collections
     * @property string $collectionClass
     */
    protected $collectionClass = 'Paybetr\PaybetrPHP\Resources\Collection';

    /**
     * the class used for resources
     * @property string $resourceClass
     */
    protected $resourceClass;

    /**
     * the class used for addresses
     * @property string $addressClass
     */
    protected $addressClass = 'Paybetr\PaybetrPHP\Resources\Address';

    /**
     * the class used for available triggers
     * @property string $availableTriggersClass
     */
    protected $availableTriggersClass = 'Paybetr\PaybetrPHP\Resources\AvailableTriggers';

    /**
     * the class used for balances
     * @property string $balanceClass
     */
    protected $balanceClass = 'Paybetr\PaybetrPHP\Resources\Balance';

    /**
     * the class used for currencies
     * @property string $currencyClass
     */
    protected $currencyClass = 'Paybetr\PaybetrPHP\Resources\Currency';

    /**
     * the class used for exchanges
     * @property string $exchangeClass
     */
    protected $exchangeClass = 'Paybetr\PaybetrPHP\Resources\Exchange';

    /**
     * the class used for transactions
     * @property string $transactionClass
     */
    protected $transactionClass = 'Paybetr\PaybetrPHP\Resources\Transaction';

    /**
     * the class used for triggers
     * @property string $triggerClass
     */
    protected $triggerClass = 'Paybetr\PaybetrPHP\Resources\Trigger';

    /**
     * the class used for webhooks
     * @property string $webhookClass
     */
    protected $webhookClass = 'Paybetr\PaybetrPHP\Resources\Webhook';

    /**
     * the class used for withdrawals
     * @property string $withdrawalClass
     */
    protected $withdrawalClass = 'Paybetr\PaybetrPHP\Resources\Withdrawal';

    /**
     * class constructor
     * @param Http\Client\HttpClient $client - an instance of HttpClient or null and one will be created
     * @param Http\Message\MessageFactory $messageFactory - an instance of MessageFactory or null and one will be created
     * @return Paybetr\PaybetrPHP\Client
     */
    public function __construct(HttpClient $client = null, MessageFactory $messageFactory = null)
    {
        $this->client = new HttpMethodsClient(
            $client ?? HttpClientDiscovery::find(),
            $messageFactory ?? MessageFactoryDiscovery::find()
        );
    }

    /**
     * static constructor
     * @param string $apiToken - paybetr api token
     * @param Http\Client\HttpClient $client - an instance of HttpClient or null and one will be created
     * @param Http\Message\MessageFactory $messageFactory - an instance of MessageFactory or null and one will be created
     * @return Paybetr\PaybetrPHP\Client
     */
    public static function factory(string $apiToken, HttpClient $client = null, MessageFactory $messageFactory = null)
    {
        $client = new static($client, $messageFactory);
        $client->setApiToken($apiToken);
        return $client;
    }

    /**
     * returns the api token
     * @return string
     */
    public function getApiToken()
    {
        return $this->apiToken;
    }

    /**
     * sets the api token
     * @param string $apiToken
     * @return Paybetr\PaybetrPHP\Client
     */
    public function setApiToken(string $apiToken)
    {
        $this->apiToken = $apiToken;
        return $this;
    }

    /**
     * returns the collection class
     * The collection class implements Paybetr\PaybetrPHP\Interfaces\CollectionInterface
     * @return string
     */
    public function getCollectionClass()
    {
        return $this->collectionClass;
    }

    /**
     * sets the collection class
     * @param string $class - The name of the collection class. The collection class must implement
     * Paybetr\PaybetrPHP\Interfaces\CollectionInterface
     * @return Paybetr\PaybetrPHP\Client
     */
    public function setCollectionClass(string $class)
    {
        if (!in_array('Paybetr\PaybetrPHP\Interfaces\CollectionInterface', class_implements($class))) {
            throw new \Exception("$class must implement Paybetr\PaybetrPHP\Interfaces\CollectionInterface", 400);
        }
        $this->collectionClass = $class;
        return $this;
    }

    /**
     * returns the resource class
     * The resource class implements Paybetr\PaybetrPHP\Interfaces\ResourceInterface
     * @return string
     */
    public function getResourceClass()
    {
        return $this->resourceClass;
    }

    /**
     * sets the resource class
     * @param string $class - The name of the resource class. The resource class must implement
     * Paybetr\PaybetrPHP\Interfaces\ResourceInterface
     * @return Paybetr\PaybetrPHP\Client
     */
    private function setResourceClass(string $class)
    {
        if (!in_array('Paybetr\PaybetrPHP\Interfaces\ResourceInterface', class_implements($class))) {
            throw new \Exception("$class must implement Paybetr\PaybetrPHP\Interfaces\ResourceInterface", 400);
        }
        $this->resourceClass = new $class($this);
        return $this;
    }

    /**
     * sets a resource class
     */
    public function setClass(string $className, string $class)
    {
        if (!in_array('Paybetr\PaybetrPHP\Interfaces\ResourceInterface', class_implements($class))) {
            throw new \Exception("$class must implement Paybetr\PaybetrPHP\Interfaces\ResourceInterface", 400);
        }
        $this->$className = $class;
        return $this;
    }

    /**
     * returns the base url
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * sets the base url
     * @param string $url
     * @return Paybetr\PaybetrPHP\Client
     */
    public function setBaseUrl(string $url)
    {
        $this->baseUrl = $url;
        return $this;
    }

    /**
     * returns the uri
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * sets the uri
     * @param string $uri
     * @return Paybetr\PaybetrPHP\Client
     */
    public function setUri(string $uri)
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * returns the full url (baseUrl + uri + queries)
     * @return string
     */
    public function getUrl()
    {
        $url = $this->getBaseUrl() . $this->getUri();
        if ($query = $this->getQuery()) {
            $url = $url .= '?' . http_build_query($query);
        }
        return $url;
    }

    /**
     * manually set the url for the api request
     * @param string $url - the url to be used
     * @return Paybetr\PaybetrPHP\Client
     */
    public function setUrl(string $url)
    {
        $this->setUri(str_replace('/api/v1/', null, parse_url($url, PHP_URL_PATH)));
        $this->clearQuery();
        if (!$query = parse_url($url, PHP_URL_QUERY)) {
            return $this;
        }
        $query = explode('&', $query);
        foreach ($query as $q) {
            $queryString = explode('=', $q);
            $this->addQuery($queryString[0], $queryString[1]);
        }
        return $this;
    }

    /**
     * returns the query property
     * @return array
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * sets the query property
     * @param array $query
     * @return Paybetr\PaybetrPHP\Client
     */
    public function setQuery(array $query = [])
    {
        $this->query = $query;
        return $this;
    }

    /**
     * adds a query parameter to the query property
     * @param string $query - the query parameter to set
     * @param string $value - the value of the query parameter
     * @return Paybetr\PaybetrPHP\Client
     */
    public function addQuery(string $query, string $value)
    {
        $this->query[$query] = $value;
        return $this;
    }

    /**
     * clears the query parameter
     * @return Paybetr\PaybetrPHP\Client
     */
    public function clearQuery()
    {
        $this->query = [];
        return $this;
    }

    /**
     * adds a filter query parameter
     * @param string $column - the column to filter on
     * @param string $value - the value of the filter
     * @return Paybetr\PaybetrPHP\Client
     */
    public function filter(string $column, string $value)
    {
        $this->addQuery('filter[' . $column . ']', $value);
        return $this;
    }

    /**
     * adds a sort query parameter
     * @param string $sort - the value for the sort parameter
     * @return Paybetr\PaybetrPHP\Client
     */
    public function sort(string $sort)
    {
        $this->addQuery('sort', $sort);
        return $this;
    }

    /**
     * adds a limit query parameter
     * @param int $limit - the number of records to return
     * @return Paybetr\PaybetrPHP\Client
     */
    public function limit(int $limit)
    {
        $this->addQuery('page[size]', $limit);
        return $this;
    }

    /**
     * resets the query parameters and uri
     * @return Paybetr\PaybetrPHP\Client
     */
    public function reset()
    {
        $this->clearQuery();
        $this->setUri('');
        return $this;
    }

    /**
     * get address resource from the api
     * @return Paybetr\PaybetrPHP\Client
     */
    public function address()
    {
        return $this->setResourceClass($this->addressClass);
    }

    /**
     * get available triggers resouce from the api
     * @return Paybetr\PaybetrPHP\Client
     */
    public function availableTriggers()
    {
        return $this->setResourceClass($this->availableTriggersClass);
    }

    /**
     * get balance resource from the api
     * @return Paybetr\PaybetrPHP\Client
     */
    public function balance()
    {
        return $this->setResourceClass($this->balanceClass);
    }

    /**
     * get currency resource from the api
     * @return Paybetr\PaybetrPHP\Client
     */
    public function currency()
    {
        return $this->setResourceClass($this->currencyClass);
    }

    /**
     * get exchange resource from the api
     * @return Paybetr\PaybetrPHP\Client
     */
    public function exchange()
    {
        return $this->setResourceClass($this->exchangeClass);
    }

    /**
     * get transaction resource from the api
     * @return Paybetr\PaybetrPHP\Client
     */
    public function transaction()
    {
        return $this->setResourceClass($this->transactionClass);
    }

    /**
     * get trigger resource from the api
     * @return Paybetr\PaybetrPHP\Client
     */
    public function trigger()
    {
        return $this->setResourceClass($this->triggerClass);
    }

    /**
     * get webhook resource from the api
     * @return Paybetr\PaybetrPHP\Client
     */
    public function webhook()
    {
        return $this->setResourceClass($this->webhookClass);
    }

    /**
     * get withdrawal resource from the api
     * @return Paybetr\PaybetrPHP\Client
     */
    public function withdrawal()
    {
        return $this->setResourceClass($this->withdrawalClass);
    }

    /**
     * return a collection
     * @return Paybetr\PaybetrPHP\Interfaces\CollectionInterface
     */
    public function get(array $arguments = [])
    {
        return $this->getCollectionClass()::collection($this, $this->getResourceClass()->get($arguments));
    }

    /**
     * return the first resource
     * @return Paybetr\PaybetrPHP\Interfaces\ResourceInterface
     */
    public function first(array $arguments = [])
    {
        $result = $this->getResourceClass()->get($arguments);
        $data = isset($result->data) ? current($result->data) : $result;
        return $this->getResourceClass()::resource($this, $data);
    }

    /**
     * return the last resource
     * @return Paybetr\PaybetrPHP\Interfaces\ResourceInterface
     */
    public function last(array $arguments = [])
    {
        $result = $this->getResourceClass()->get($arguments);
        $data = isset($result->data) ? end($result->data) : $result;
        return $this->getResourceClass()::resource($this, $data);
    }

    /**
     * find a resource by its identifier
     * @param string $identifier - the identifier for the resource
     * @return Paybetr\PaybetrPHP\Interfaces\ResourceInterface
     */
    public function find(string $identifier = null)
    {
        return $this->getResourceClass()::resource($this, $this->getResourceClass()->find($identifier));
    }

    /**
     * create a resource
     * @param array $attributes - the attributes needed to create the resource
     * @return Paybetr\PaybetrPHP\Interfaces\ResourceInterface
     */
    public function create(array $attributes = [])
    {
        return $this->getResourceClass()::resource($this, $this->getResourceClass()->create($attributes));
    }

    /**
     * update a resource
     * @param array $attributes - attributes that should be updated
     * @return Paybetr\PaybetrPHP\Interfaces\ResourceInterface
     */
    public function update(array $attributes = [])
    {
        return $this->getResourceClass()::resource($this, $this->getResourceClass()->update($identifier, $attributes));
    }

    /**
     * delete a resource
     * @return void
     */
    public function delete()
    {
        $this->getResourceClass()->delete();
    }

    /**
     * send a request to the api
     * @param string $method - HTTP method (GET, POST, PUT, etc.)
     * @param array $body - body to send
     * @return object
     */
    public function request($method = 'GET', array $body = [])
    {
        $url = $this->getUrl();
        $headers = [
            'Authorization' => 'Bearer ' . $this->getApiToken(),
            'Accept' => 'application/vnd.api+json',
        ];
        $response = $this->client->send($method, $url, $headers, http_build_query($body));
        $statusCode = $response->getStatusCode();
        /**
         * 2020-01-21: Exceptions disabled because it breaks code before
         * a response is returned to client like Drupal to handle error correctly.
         * Better for now to just return entire response to allow Drupal to process
         */
        // if ($statusCode >= 400 && $statusCode < 500) {
        //     throw new ClientError($response->getReasonPhrase(), $statusCode);
        // }
        // if ($statusCode >= 500 && $statusCode < 600) {
        //     throw new ServerError($response->getReasonPhrase(), $statusCode);
        // }
        // if ($statusCode < 200 || $statusCode > 299) {
        //     throw new \Exception($response->getReasonPhrase(), $response->getStatusCode());
        // }
        return json_decode($response->getBody()->getContents());
    }
}
