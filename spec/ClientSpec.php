<?php

namespace spec\Paybetr\PaybetrPHP;

use Paybetr\PaybetrPHP\Client;
use Paybetr\PaybetrPHP\Resources\Address;
use PhpSpec\ObjectBehavior;
use Http\Client\HttpClient;
use Http\Message\MessageFactory;
use Paybetr\PaybetrPHP\Resources\AvailableTriggers;
use Paybetr\PaybetrPHP\Resources\Balance;
use Paybetr\PaybetrPHP\Resources\Currency;
use Paybetr\PaybetrPHP\Resources\Exchange;
use Paybetr\PaybetrPHP\Resources\Transaction;
use Paybetr\PaybetrPHP\Resources\Trigger;
use Paybetr\PaybetrPHP\Resources\Webhook;
use Paybetr\PaybetrPHP\Resources\Withdrawal;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Paybetr\PaybetrPHP\Resources\Collection;

class ClientSpec extends ObjectBehavior
{
    public function let(HttpClient $client, MessageFactory $messageFactory)
    {
        $this->beConstructedWith($client, $messageFactory);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(Client::class);
    }

    public function it_can_get_and_set_the_api_token()
    {
        $this->getApiToken()->shouldReturn(null);
        $this->setApiToken('foo')->shouldReturn($this);
        $this->getApiToken()->shouldReturn('foo');
    }

    public function it_can_set_and_retrieve_the_base_url()
    {
        $this->getBaseUrl()->shouldReturn('https://api.paybetr.com/api/v1/');
        $this->setBaseUrl('https://www.google.com');
        $this->getBaseUrl()->shouldReturn('https://www.google.com');
    }

    public function it_can_get_and_set_the_collection_class()
    {
        $this->setCollectionClass('Paybetr\PaybetrPHP\Resources\Collection')->shouldReturn($this);
        $this->getCollectionClass()->shouldReturn('Paybetr\PaybetrPHP\Resources\Collection');
    }

    public function it_can_override_a_resource_class()
    {
        $this->setClass('addressClass', 'Paybetr\PaybetrPHP\Resources\Balance');
        $this->address()->shouldReturn($this);
        $this->getResourceClass()->shouldHaveType(Balance::class);
    }

    public function it_can_set_the_address_resource_class()
    {
        $this->getResourceClass()->shouldReturn(null);
        $this->address()->shouldReturn($this);
        $this->getResourceClass()->shouldHaveType(Address::class);
    }

    public function it_can_set_the_available_triggers_resource_class()
    {
        $this->getResourceClass()->shouldReturn(null);
        $this->availableTriggers()->shouldReturn($this);
        $this->getResourceClass()->shouldHaveType(AvailableTriggers::class);
    }

    public function it_can_set_the_balance_resource_class()
    {
        $this->getResourceClass()->shouldReturn(null);
        $this->balance()->shouldReturn($this);
        $this->getResourceClass()->shouldHaveType(Balance::class);
    }

    public function it_can_set_the_currency_resource_class()
    {
        $this->getResourceClass()->shouldReturn(null);
        $this->currency()->shouldReturn($this);
        $this->getResourceClass()->shouldHaveType(Currency::class);
    }

    public function it_can_set_the_exchange_resource_class()
    {
        $this->getResourceClass()->shouldReturn(null);
        $this->exchange()->shouldReturn($this);
        $this->getResourceClass()->shouldHaveType(Exchange::class);
    }

    public function it_can_set_the_transaction_resource_class()
    {
        $this->getResourceClass()->shouldReturn(null);
        $this->transaction()->shouldReturn($this);
        $this->getResourceClass()->shouldHaveType(Transaction::class);
    }

    public function it_can_set_the_trigger_resource_class()
    {
        $this->getResourceClass()->shouldReturn(null);
        $this->trigger()->shouldReturn($this);
        $this->getResourceClass()->shouldHaveType(Trigger::class);
    }

    public function it_can_set_the_webhook_resource_class()
    {
        $this->getResourceClass()->shouldReturn(null);
        $this->webhook()->shouldReturn($this);
        $this->getResourceClass()->shouldHaveType(Webhook::class);
    }

    public function it_can_set_the_withdrawal_resource_class()
    {
        $this->getResourceClass()->shouldReturn(null);
        $this->withdrawal()->shouldReturn($this);
        $this->getResourceClass()->shouldHaveType(Withdrawal::class);
    }

    public function it_can_get_and_set_the_uri()
    {
        $this->setUri('newuri')->shouldReturn($this);
        $this->getUri()->shouldReturn('newuri');
    }

    public function it_can_get_and_set_the_url()
    {
        $this->setUrl('https://api.paybetr.com/api/v1/currencies')->shouldReturn($this);
        $this->getUrl()->shouldReturn('https://api.paybetr.com/api/v1/currencies');
        $this->setUrl('https://api.paybetr.com/api/v1/currencies?sort=-created_at')->shouldReturn($this);
        $this->getUrl()->shouldReturn('https://api.paybetr.com/api/v1/currencies?sort=-created_at');
        $this->getQuery()->shouldReturn(['sort' => '-created_at']);
    }

    public function it_can_get_set_add_and_clear_the_query()
    {
        $this->setQuery(['sort' => '-created_at'])->shouldReturn($this);
        $this->getQuery()->shouldReturn(['sort' => '-created_at']);
        $this->addQuery('filter[symbol]', 'BTC')->shouldReturn($this);
        $this->getQuery()->shouldReturn(['sort' => '-created_at', 'filter[symbol]' => 'BTC']);
        $this->clearQuery()->shouldReturn($this);
        $this->getQuery()->shouldReturn([]);
    }

    public function it_can_add_filters_to_the_query()
    {
        $this->filter('symbol', 'BTC')->shouldReturn($this);
        $this->getQuery()->shouldReturn(['filter[symbol]' => 'BTC']);
    }

    public function it_can_add_sort_to_the_query()
    {
        $this->sort('created_at')->shouldReturn($this);
        $this->getQuery()->shouldReturn(['sort' => 'created_at']);
    }

    public function it_can_add_limit_to_the_query()
    {
        $this->limit(1)->shouldReturn($this);
        $this->getQuery()->shouldReturn(['page[size]' => '1']);
    }

    public function it_can_reset_query_and_uri()
    {
        $this->setUri('test')->shouldReturn($this);
        $this->getUri()->shouldReturn('test');
        $this->addQuery('sort', 'created_at')->shouldReturn($this);
        $this->getQuery()->shouldReturn(['sort' => 'created_at']);
        $this->reset()->shouldReturn($this);
        $this->getUri()->shouldReturn('');
    }

    public function it_can_make_an_http_request(HttpClient $client, MessageFactory $messageFactory, RequestInterface $request, ResponseInterface $response, StreamInterface $stream)
    {
        $this->beConstructedWith($client, $messageFactory);
        $this->setApiToken('foo');
        $data = json_encode([
            'data' => [
                [
                    'type' => 'addresses',
                    'id' => '4ed3d43c-dda5-4e2e-9340-70d427aac94a',
                    'attributes' => [
                        'currency' => 'XMR',
                        'name' => 'Monero',
                        'address' => '8BzUYhmB1y3ZWPMsEyEgRGgG6UQVP7pnphvmRgMBNha7Fm98PCcfZsihr1MVS9G2LHHnY9w8D9Xzu2pDdqQxh17bU82xxSR',
                        'external_id' => null,
                        'created_at' => '2018-08-23T10:04:20+00:00',
                        'updated_at' => '2018-08-23T10:04:20+00:00',
                    ],
                    'links' => [
                        'self' => 'https://api.paybetr.com/api/v1/addresses/8BzUYhmB1y3ZWPMsEyEgRGgG6UQVP7pnphvmRgMBNha7Fm98PCcfZsihr1MVS9G2LHHnY9w8D9Xzu2pDdqQxh17bU82xxSR',
                        'qrcode' => '"https://api.paybetr.com/addresses/8BzUYhmB1y3ZWPMsEyEgRGgG6UQVP7pnphvmRgMBNha7Fm98PCcfZsihr1MVS9G2LHHnY9w8D9Xzu2pDdqQxh17bU82xxSR/qrcode.png',
                    ],
                ],
                [
                    'type' => 'addresses',
                    'id' => '4ed3d43c-dda5-4e2e-9340-70d427aac94b',
                    'attributes' => [
                        'currency' => 'XMR',
                        'name' => 'Monero',
                        'address' => '8BzUYhmB1y3ZWPMsEyEgRGgG6UQVP7pnphvmRgMBNha7Fm98PCcfZsihr1MVS9G2LHHnY9w8D9Xzu2pDdqQxh17bU82xxSS',
                        'external_id' => null,
                        'created_at' => '2018-08-23T10:04:20+00:00',
                        'updated_at' => '2018-08-23T10:04:20+00:00',
                    ],
                    'links' => [
                        'self' => 'https://api.paybetr.com/api/v1/addresses/8BzUYhmB1y3ZWPMsEyEgRGgG6UQVP7pnphvmRgMBNha7Fm98PCcfZsihr1MVS9G2LHHnY9w8D9Xzu2pDdqQxh17bU82xxSS',
                        'qrcode' => '"https://api.paybetr.com/addresses/8BzUYhmB1y3ZWPMsEyEgRGgG6UQVP7pnphvmRgMBNha7Fm98PCcfZsihr1MVS9G2LHHnY9w8D9Xzu2pDdqQxh17bU82xxSS/qrcode.png',
                    ],
                ],
            ],
            'links' => [
                'first' => 'https://api.paybetr.com/api/v1/currencies/xmr/addresses?page[number]=1',
                'last' => 'https://api.paybetr.com/api/v1/currencies/xmr/addresses?page[number]=1',
                'prev' => null,
                'next' => null,
                'self' => 'https://api.paybetr.com/api/v1/currencies/xmr/addresses',
            ],
            'meta' => [
                'current_page' => 1,
                'from' => 1,
                'last_page' => 1,
                'path' => 'https://api.paybetr.com/api/v1/currencies/xmr/addresses',
                'per_page' => 30,
                'to' => 2,
                'total' => 2,
            ],
        ]);
        $messageFactory->createRequest('GET', 'https://api.paybetr.com/api/v1/currencies/XMR/addresses', ['Authorization' => 'Bearer foo', 'Accept' => 'application/vnd.api+json'], '')->willReturn($request);
        $client->sendRequest($request)->willReturn($response);
        $response->getStatusCode()->willReturn(200);
        $response->getBody()->willReturn($stream);
        $stream->getContents()->willReturn($data);
        $this->address()->get(['symbol' => 'XMR'])->shouldHaveType(Collection::class);
        $this->address()->first(['symbol' => 'XMR'])->shouldHaveType(Address::class);
    }
}
